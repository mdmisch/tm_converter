
#include <stdio.h>
#include <stdlib.h>
#include "string.h"

#include "tm_modules.h"

#define INPUT_LENGTH 1024

#define ASSERT_TRUE_FILEFORMAT_ERROR(b,l) if (!b) \
	{\
		fprintf(stderr, "Incorrect input: %lli\n", l);\
		free(line);\
		return -1;\
	}

int onisch_parser(	struct container* c,
					FILE* f,
					parser_callback_func callback,
					int argc,
					char** argv
				)
{
	char* line = malloc(INPUT_LENGTH);
	
	for (	unsigned long long int cur_line = 1;
			fgets(line, INPUT_LENGTH, f);
			cur_line++
		)
		if ((*line != 0) &&
			(*line != '\n') &&
			(*line != '\r')
		)
		{
			char* chr_before = line;	/* "character" before */
			char* chr_after;			/* "character" after */		
			char* direction;
			char* end_of_line;

			char* st_from;
			char* st_to;
			
			/* First "character" may be a whitespace. */
			
			st_from = strchr(line+1,' ');
			ASSERT_TRUE_FILEFORMAT_ERROR(st_from, cur_line);
			*((st_from)++) = 0;
			
			st_to = strchr(st_from, ':');
			ASSERT_TRUE_FILEFORMAT_ERROR(st_to, cur_line);
			*(st_to++) = 0;
			
			chr_after = strchr(st_to, ' ');
			ASSERT_TRUE_FILEFORMAT_ERROR(chr_after, cur_line);
			*(chr_after++) = 0;
			
			
			direction = chr_after;
			if (*direction == ' ')
				direction++;
			/* And second "character" may be a whitespace too. */
			
			direction = strchr(direction, ' ');
			ASSERT_TRUE_FILEFORMAT_ERROR(direction, cur_line);
			*(direction++) = 0;
			
			if (end_of_line = strchr(direction, '\n'))
				*end_of_line = 0;
			if (end_of_line = strchr(direction, '\r'))
				*end_of_line = 0;

			if  (	!(	(*callback)	(	c,
										st_from,
										st_to,
										chr_before,
										chr_after,
										direction
									)
					)
				)
			{
				free(line);
				return 0;
			}
			
		}
	free(line);
	return 1;
}

struct parser onisch_parser_description = {
	&onisch_parser,
	"onisch",
	"A parser for Onischenko's turing machine emulator syntax.\n"
	"    Pure text, not rtf\n"
	"\n"
	"    A st_from:st_to B D\n"
	"\n"
	"    A - character before\n"
	"    B - character after\n"
	"    D - direction\n"
	"\n"
	"    Character may be a whitespace."
};
