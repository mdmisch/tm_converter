
#include <stdio.h>
#include <stdlib.h>
#include "string.h"

#include "tm_modules.h"

#define INPUT_LENGTH 1024

#define ASSERT_TRUE_FILEFORMAT_ERROR(b,l) if (!b) \
	{\
		fprintf(stderr, "Incorrect input: %lli\n", l);\
		free(line);\
		return -1;\
	}

int arrow_parser(	struct container* container,
					FILE* f,
					parser_callback_func callback,
					int argc,
					char** argv
					)
{
	char* line = malloc(INPUT_LENGTH);
	
	char chr_before[2] = {0, 0};	/* "character" before */
	char chr_after[2] = {0, 0};	/* "character" after */	
	char direction[2] = {0, 0};
	
	char* st_from = line + 1;
	char* st_to;
	
	for (	unsigned long long int cur_line = 1;
			fgets(line, INPUT_LENGTH, f);
			cur_line++
		)
	{
		char* comment_begin = strstr(line, "//");
		if (comment_begin)
			*comment_begin = 0;
		
		comment_begin = strchr(line, ' ');
		if (comment_begin)
			*comment_begin = 0;
		
		if ((*line != 0) &&
			(*line != '\n') &&
			(*line != '\r')
		)
		{

			char* end_of_line;

			chr_before[0] = line[0];
			
			st_to = strstr(line, "->");
			ASSERT_TRUE_FILEFORMAT_ERROR(st_to, cur_line);
			*(st_to++) = 0;
			
			chr_after[0] = *(++st_to);
			
			ASSERT_TRUE_FILEFORMAT_ERROR(chr_after[0], cur_line);
			
			st_to++;
			
			if (end_of_line = strchr(st_to, '\n'))
				*end_of_line = 0;
			if (end_of_line = strchr(st_to, '\r'))
				*end_of_line = 0;
			
			ASSERT_TRUE_FILEFORMAT_ERROR(*st_to, cur_line);
			
			end_of_line = strchr(st_to, '\0');
			direction[0] = *(--end_of_line);
			*end_of_line = '\0';

			if  (	!(	(*callback)	(	container,
										st_from,
										st_to,
										chr_before,
										chr_after,
										direction
									)
					)
				)
			{
				free(line);
				return 0;
			}
		}
	}
	free(line);
	return 1;
}

struct parser arrow_parser_definition = {
	&arrow_parser,
	"arrow",
	"A parser for file format of one of online emulators.\n"
	"    Command must not contain whitespaces. Comments begins with\n"
	"    double backslashes (\"//\"). Whitespaces and comments after\n"
	"    end of command is allowed.\n"
	"\n"
	"    Astate_from->Bstate_toD\n"
	"\n"
	"    A - character before. Only one character\n"
	"        at the beginning of line\n"
	"    B - character after. First character after \"->\"\n"
	"    D - direction. The last character."
};
