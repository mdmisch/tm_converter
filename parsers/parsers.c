
#include <stddef.h>

#include "parsers/dikarev_parser.h"
#include "parsers/arrow_parser.h"
#include "parsers/onisch_parser.h"
#include "parsers/space_parser.h"

struct parser *parsers[] = {
	&dikarev_parser_definition,
	&arrow_parser_definition,
	&onisch_parser_description,
	&space_parser_definition,
	NULL
};
