
#include <stdio.h>
#include <stdlib.h>
#include "string.h"

#include "tm_modules.h"

#define INPUT_LENGTH 1024

#define ASSERT_TRUE_FILEFORMAT_ERROR(b,l) if (!b) \
	{\
		fprintf(stderr, "Incorrect input: %lli\n", l);\
		free(line);\
		return -1;\
	}

int dikarev_parser(	struct container* c,
					FILE* f,
					parser_callback_func callback,
					int argc,
					char** argv
					)
{
	char* line = malloc(INPUT_LENGTH);

	for (	unsigned long long int cur_line = 1;
		fgets(line, INPUT_LENGTH, f);
		cur_line++
	)
	if ((*line != 0) &&
		(*line != '\n') &&
		(*line != '\r') &&
		(*line != '#')
	)
	{
		char* st_from = line;
		char* st_to;
		char* chr_before;	/* "character" before */
		char* chr_after;	/* "character" after */		
		char* direction;
		char* end_of_line;

		chr_before = strchr(line,',');
		ASSERT_TRUE_FILEFORMAT_ERROR(chr_before, cur_line);
		*(chr_before++) = 0;
		
		st_to = strchr(chr_before, ':');
		ASSERT_TRUE_FILEFORMAT_ERROR(st_to, cur_line);
		*(st_to++) = 0;
		
		chr_after = strchr(st_to, ',');
		ASSERT_TRUE_FILEFORMAT_ERROR(chr_after, cur_line);
		*(chr_after++) = 0;
		
		direction = strchr(chr_after, ',');
		ASSERT_TRUE_FILEFORMAT_ERROR(direction, cur_line);
		*(direction++) = 0;
		
		if (end_of_line = strchr(direction, '\n'))
			*end_of_line = 0;
		if (end_of_line = strchr(direction, '\r'))
			*end_of_line = 0;
		
		if  (	!(	(*callback)	(	c,
									st_from,
									st_to,
									chr_before,
									chr_after,
									direction
								)
				)
			)
		{
			free(line);
			return 0;
		}
	}
	
	free(line);
	return 1;	
}

struct parser dikarev_parser_definition = {
	dikarev_parser,
	"dik",
	"A parser for the Dikarev's file format. (Pure text, not rtf.)\n"
	"\n"
	"    state_from,chr_before:state_to,chr_after,direction"
};
