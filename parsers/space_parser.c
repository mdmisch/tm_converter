
#include <stdio.h>
#include <stdlib.h>
#include "string.h"

#include "tm_modules.h"

#define INPUT_LENGTH 1024

#define ASSERT_TRUE_FILEFORMAT_ERROR(b,l) if (!b) \
	{\
		fprintf(stderr, "Incorrect input: %lli\n", l);\
		free(line);\
		return -1;\
	}

int space_parser (	struct container* c,
					FILE* f,
					parser_callback_func callback,
					int argc,
					char** argv
				 )
{
	char* line = malloc(INPUT_LENGTH);
		
	for (	unsigned long long int cur_line = 1;
			fgets(line, INPUT_LENGTH, f);
			cur_line++
		)
		if ((*line != 0) &&
			(*line != '\n') &&
			(*line != '\r') &&
			(*line != ';')
		)
		{
			char* chr_before;	/* "character" before */
			char* chr_after;	/* "character" after */		
			char* direction;
			char* end_of_line;
			
			char* st_from = line + strspn(line, " ");
			char* st_to;

			chr_before = line + strcspn(line," \t");
			ASSERT_TRUE_FILEFORMAT_ERROR(chr_before, cur_line);
			*(chr_before++) = 0;
			chr_before += strspn(chr_before, " \t");
			
			chr_after = chr_before + strcspn(chr_before, " \t");
			ASSERT_TRUE_FILEFORMAT_ERROR(chr_after, cur_line);
			*(chr_after++) = 0;
			chr_after += strspn(chr_after, " \t");
			
			direction = chr_after + strcspn(chr_after, " \t");
			ASSERT_TRUE_FILEFORMAT_ERROR(direction, cur_line);
			*(direction++) = 0;
			direction += strspn(direction, " \t");
			
			st_to = direction + strcspn(direction, " \t");
			ASSERT_TRUE_FILEFORMAT_ERROR(st_to, cur_line);
			*(st_to++) = 0;
			st_to += strspn(st_to, " \t");
			
			end_of_line = st_to + strcspn(st_to, " \t\r\n");
			*end_of_line = 0;
			
			if  (	!(	(*callback)	(	c,
										st_from,
										st_to,
										chr_before,
										chr_after,
										direction
									)
					)
				)
			{
				free(line);
				return 0;
			}
		
		}
	
	free(line);
	return 1;
}

struct parser space_parser_definition = {
	&space_parser,
	"space",
	"A parser for one's of online emulators syntax.\n"
	"\n"
	"    st_from chr_from chr_to direction st_to"
};
