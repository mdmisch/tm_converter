
#include <string.h>

#include "tm_modules.h"

#include "exporters/exporters.h"
#include "containers/containers.h"

struct exporter* get_exporter_by_name(char* name)
{
	if (name == NULL)
		return NULL;
	
	unsigned int i;
	for (	i = 0;
			(exporters[i] != NULL) &&
			(strcmp(exporters[i]->name, name));
			i++
		);
	return exporters[i];
}

int print_exporter_description(char* name, FILE* output)
{
	if (name)
	{
		struct exporter* e = get_exporter_by_name(name);
		if (e == NULL)
		{
			fputs(name, output);
			fputs(" - Unknown exporter.\n", output);
			return 1;
		}
		else
		{
			fputs(e->name, output);
			fputs(" - ", output);
			fputs(e->description, output);
			fputc('\n', output);
			return 1;
		}
	}
	else
	{
		for	(	int i = 0;
				exporters[i] != NULL;
				i++
			)
		{
			fputs(exporters[i]->name, output);
			fputs(" - ", output);
			fputs(exporters[i]->description, output);
			fputs("\n\n", output);
		}
		return 1;
	}
}

int verify_container_support(struct exporter* e, int id)
{
	if ((e == NULL) || (id == 0))
		return 0;
	
	for (	int* cur_id = *(e->container_types);
			*cur_id != NULL_CONTAINER; 
			cur_id++
		)
		if (*cur_id == id)
			return 1;
	
	return 0;
}
