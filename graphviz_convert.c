
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <glib.h>

#include "tm_modules.h"

#include "param_list.h"

#define CLOSE_ALL		 	fclose(fin);\
							fclose(fout);




FILE *fin;
FILE *fout;





char* help_msg =
	"Usage: %s [KEY]... [INPUT_FILE]... [OUTPUT_FILE]\n"
	"Create graph for a Turing machine in dot file format.\n"
	"\n"
	"  -p PARSER    choose a parser\n"
	"  -c CONTAINER choose a container\n"
	"  -e EXPORTER  choose an exporter\n"
	"  -i INPUT     set input filename\n"
	"  -o OUTPUT    set output filename\n"
	"  -dP			print all parsers' names with their descriptions\n"
	"  -dp PARSER   print parsers' description\n"
	"  -dC			print all contaiers' names with their descriptions\n"
	"  -dc PARSER   print container's description\n"
	"  -dE			print all exporters' names with their descriptions\n"
	"  -de PARSER   print exporter's description\n"
	"  -pp PARAM1,PARAM2...   parameters for parser\n" 
	"  -ep PARAM1,PARAM2...   parameters for exporter\n"
	"  -h, --help   print this message\n"
	"\n"
	"File names can be defined after \"-i\" and \"-o\"\n"
	"or without them, but not with and without them at the same time.\n"
	"\n"
	"\"-\" without \"-i\" or \"-o\" means stdin or stdout.\n";
	

int main (int argc, char** argv)
{
	char* parser_name = "dik";
	struct container_descr* c_descr = NULL;
	char* exporter_name = "graphviz";
	
	struct tmp_param_list* parser_params = param_list_create();
	struct tmp_param_list* exporter_params = param_list_create();
	
	if (*argv == NULL)
	{
		fputs("Why is there no params at all?",stderr);
		return 3;
	}
	
	char* binary_name = *argv;
	
	unsigned int filename_counter = 0;
	int param_defined = 0;
	
	char* input_fname = NULL;
	char* output_fname = NULL;
	
	for (argv++; *argv != NULL; argv++)
	{

		if (strcmp(*argv, "-p") == 0)
		{
			parser_name = *(++argv);
			if (!parser_name)
			{
				fputs("Hey, where is a parser?\n", stderr);
				return 3;
			}
		}
		else if (strcmp(*argv, "-c") == 0)
		{
			if (*(++argv) == NULL)
			{
				fputs(	"Please, define container name after \"-c\".",
						stderr
					 );
				return 3;
			}
			c_descr = find_container_descr_by_name(*argv);
			
			if (c_descr == NULL)
			{
				fputs("There is no container with this name.", stderr);
				return 4;
			}
		}
		else if (strcmp(*argv, "-e") == 0)
		{
			if (*(++argv) == NULL)
			{
				fputs(	"Please, define exporter name after \"-e\".",
						stderr
					 );
				return 3;
			}
			exporter_name = *argv;
		}
		else if (strcmp(*argv, "-i") == 0)
		{
			param_defined = 1;
			input_fname = *(++argv);
			if (!input_fname)
			{
				fputs("So, from where I need to read?\n", stderr);
				return 3;
			}
		}
		else if (strcmp(*argv, "-o") == 0)
		{
			param_defined = 1;
			output_fname = *(++argv);
			if (!output_fname)
			{
				fputs("Please, define output, at least.\n", stderr);
				return 4;
			}
		}
		else if (	(strcmp(*argv, "-h") == 0) ||
					(strcmp(*argv, "--help") == 0)
				)
		{
			printf(help_msg, binary_name);
			return 0;
		}
		else if	(strcmp(*argv, "-dP") == 0)
		{
			print_parser_description(NULL, stdout);
			return 0;
		}
		else if (strcmp(*argv, "-dp") == 0)
		{
			char* name = *(++argv);
			if (name == NULL)
			{
				fputs(	"Error while parsing \"-dp\". Add parser name.\n",
						stderr
					);
				return 4;
			}
			
			if (print_parser_description(name, stdout))
				return 2;
			else
				return 0;
		}
		else if (strcmp(*argv, "-dC") == 0)
		{
			if (print_container_descr_by_name(NULL, stdout))
				return 2;
			else
				return 0;
		}
		else if (strcmp(*argv, "-dc") == 0)
		{
			char* name = *(++argv);
			if (name == NULL)
			{
				fputs(	"Error while parsing \"-dc\". "
						"Add container name.\n",
						stderr
					);
				return 4;
			}
			
			if (print_container_descr_by_name(name, stdout))
				return 2;
			else
				return 0;
		}
		else if (strcmp(*argv, "-dE") == 0)
		{
			if (!print_exporter_description(NULL, stdout))
				return 2;
			else
				return 0;
		}
		else if (strcmp(*argv, "-de") == 0)
		{
			char* name = *(++argv);
			if (name == NULL)
			{
				fputs(	"Error while parsing \"-dc\". "
						"Add exporter name.\n",
						stderr
					);
				return 4;
			}
			
			if (!print_exporter_description(name, stdout))
				return 2;
			else
				return 0;
		}
		else if (strcmp(*argv, "-pp") == 0)
		{
			char* params = *(++argv);
			if (params == NULL)
			{
				fputs("After \"-pp\" must be parser's params.\n", stderr);
				return 4;
			}
			
			if (!param_list_add_params(parser_params, params))
			{
				fputs(	"Something is wrong with parser's params.\n",
						stderr
					);
				return 5;
			}
		}
		else if (strcmp(*argv, "-ep") == 0)
		{
			char* params = *(++argv);
			if (params == NULL)
			{
				fputs(	"After \"-ep\" must be exporter's params.\n",
						stderr
					 );
				return 4;
			}
			
			if (!param_list_add_params(exporter_params, params))
			{
				fputs(	"Something is wrong with exporter's params.\n",
						stderr
					 );
				return 5;
			}
		}
		else if (!param_defined)
		{
			switch (filename_counter) {
				case 0: /* input */
					if (strcmp(*argv, "-") == 0)
						input_fname = NULL;
					else
						input_fname = *argv;
					break;
				case 1: /* output */
					if (strcmp(*argv, "-") == 0)
						output_fname = NULL;
					else
						output_fname = *argv;
					break;
				default:
					fputs("Too many files, don't you think?\n", stderr);
					return 8;
			}
			filename_counter++;
		}
		else
		{
			fputs(	"Er... ror... You... have already strted... "
					"to define input and output by...\n"
					"options. ...\n"
					"Bra-a-ains...\n",
					stderr
				);
			return 3;
		}
	}

	struct parser* p = get_parser_by_name(parser_name);
	if (!p)
	{
		fprintf(stderr,
				"I don't know this parser: %s\n",
				parser_name
				);
		return 3;
	}
	
	struct exporter* e = get_exporter_by_name(exporter_name);
	if (!e)
	{
		fprintf(stderr,
				"I don't know this exporter: %s\n",
				exporter_name
				);
		return 3;
	}
	
	if (!c_descr) 
	{
		c_descr = find_container_descr((*(e->container_types))[0]);
		if (c_descr == NULL)
		{
			fputs(	"Something has gone wrong. Found container "
					"description address is NULL.\n",
					stderr
				);
			return 7;
		}
	}
	else
	{
		if (!verify_container_support(e, c_descr->type))
		{
			fputs("This container is not supported.\n", stderr);
			return 6;
		}
	}
	
	if (!input_fname)
		fin = stdin;
	else if (!(fin = fopen(input_fname, "r")))
	{
		perror("Input file");
		return 1;
	}
	
	if (!output_fname)
		fout = stdout;
	else if (!(fout = fopen(output_fname, "w")))
	{
		fclose(fin);
		perror("Output file");
		return 1;
	}
	
	/*~~~~~~~~~~~~~~~~*/	
	struct container* cont = gen_container_by_descr(c_descr);
	
	if (cont == NULL)
	{
		fputs(	"Something has gone wrong. Generated container's "
				"pointer\n" "is NULL\n",
				stderr
			 );
		return 6;
	}

	int parser_argc;
	char** parser_argv;
	param_list_convert(&parser_params, &parser_argc, &parser_argv);

	if (	!(*(p->func))(	cont,
							fin,
							c_descr->filler,
							parser_argc,
							parser_argv
						 )
		)
	{
		fputs("Parser's function has failed.\n", stderr);
		if(! c_descr->destructor(cont))
			fputs("Oups, container's destructor failed too.\n", stderr);
		free_argv(&parser_argv);
		CLOSE_ALL;
		return 2;
	}
	
	
	int exporter_argc;
	char** exporter_argv;
	param_list_convert(&exporter_params, &exporter_argc, &exporter_argv);
	
	(*(e->func)) (cont, fout, exporter_argc, exporter_argv);
	
	/*~~~~~~~~~~~~~~~~*/
	free_argv(&parser_argv);
	free_argv(&exporter_argv);
	CLOSE_ALL;
	return 0;
}
