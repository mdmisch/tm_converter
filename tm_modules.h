
#ifndef TM_MODULES
#define TM_MODULES

#include <stdio.h>

struct container {
	int type;
};

typedef int (*exporter_func) (	struct container* c,
								FILE* f,
								int argc,
								char** argv
							 );

typedef int (*container_destroy_func) (struct container* c);
typedef struct container* (*container_construct_func) (void);

typedef int (*parser_callback_func) (	struct container* c,
										char* st_from,
										char* st_to,
										char* chr_before,
										char* chr_after,
										char* direction
									);

typedef int (*parser_func) (struct container* c,
							FILE* f,
							parser_callback_func callback,
							int argc,
							char** argv
							);


struct exporter {
	char* name;
	char* description;
	int (*container_types)[];
	exporter_func func;
};

struct container_descr{
	int type;
	parser_callback_func filler;
	container_construct_func constructor;
	container_destroy_func destructor;
	char* name;
	char* description;
};

struct parser
{
	parser_func func;
	char* name;
	char* description;
};

struct exporter* get_exporter_by_name(char* name);
int print_exporter_description(char* name, FILE* output);


int verify_container_support(struct exporter* e, int id);

struct container* gen_container_by_type
	(	int type,
		struct container_descr** descr
	);

struct container_descr* find_container_descr (int type);
struct container_descr* find_container_descr_by_name (char* name);
struct container* 
	gen_container_by_descr (struct container_descr* descr);

int print_container_descr_by_name(char* name, FILE* fout);


int print_parser_description(char* name, FILE* output);

struct parser* get_parser_by_name(char* name);

#endif
