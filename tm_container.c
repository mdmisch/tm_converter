
#include <stddef.h>
#include <string.h>

#include "tm_modules.h"

#include "containers/containers.h"

struct container_descr* find_container_descr (int type)
{
	struct container_descr** descr;
	for	(	descr = containers;
			*descr != NULL &&
			(*descr)->type != type;
			descr++
		);
	
	return *descr;
}

struct container_descr* find_container_descr_by_name (char* name)
{
	if (name == NULL)
		return NULL;
	
	struct container_descr** descr;
	for	(	descr = containers;
			*descr != NULL &&
			(strcmp((*descr)->name, name) == 0);
			descr++
		);
	
	return *descr;

}

struct container* 
	gen_container_by_descr (struct container_descr* descr)
{
	if (descr == NULL)
		return NULL;
	return (*descr->constructor) ();
}

struct container* gen_container_by_type 
	(	int type,
		struct container_descr** descr
	)
{
	struct container_descr* found_descr =
		find_container_descr(type);
	
	if (found_descr == NULL)
	{
		if (descr != NULL)
			*descr = NULL ;
		return NULL;
	}
	else
	{
		if (descr != NULL)
			*descr = found_descr;
		return (*(found_descr->constructor)) ();
	}
}

int print_container_descr_by_name(char* name, FILE* fout)
{
	if (name == NULL)
	{
		for (int i = 0; containers[i] != NULL; i++)
		{
			fputs(containers[i]->name, fout);
			fputs(" - ", fout);
			fputs(containers[i]->description, fout);
			fputc('\n', fout);
		}
	}
	else
	{
		struct container_descr* descr = find_container_descr_by_name(name);
		if (descr != NULL)
		{
			fputs(descr->name, fout);
			fputs(" - ", fout);
			fputs(descr->description, fout);
		}
	}
	
	return 0;
}
