
#include "stddef.h"
#include "stdio.h"

#include "tm_modules.h"
#include "containers/containers.h"
#include "containers/labeled_digraph.h"

#define ERROR(str) fputs("graphviz_exporter error: " str "\n", stderr)

struct output_opts {
	FILE* fout;
	int print_weight;
	unsigned long long int max_link_count;
};

void write_link(struct link* l,
				struct link_desc* desc,
				struct output_opts* op
				)
{
	fprintf(op->fout,
			"\"%s\" -> \"%s\" \[label=\"",
			desc->st_from_opts->pseudo,
			desc->st_to_opts->pseudo
		);
	if (desc->first != NULL)
	{
		struct char_seq* sq = desc->first;
		fprintf(	op->fout,
				"%s -> %s %s",
				sq->chr_before,
				sq->chr_after,
				sq->direction
			);
		sq = sq->next;
		
		for (; sq != NULL; sq = sq->next)
		{
			fprintf(	op->fout,
						"\\n%s -> %s %s",
						sq->chr_before,
						sq->chr_after,
						sq->direction
					);
		}
	}
	else
		ERROR("No char sequences on link!");
	
	if (op->print_weight)
		fprintf(	op->fout,
					"\",weight=%llu];\n",
					op->max_link_count*2 + 1
					 - desc->st_from_opts->links
					 - desc->st_to_opts->links
			);
	else
		fputs("\"];\n", op->fout);
}

void write_state(	char* state_name,
					struct state_options* st_opts, 
					struct output_opts* op
				)
{
	fprintf(	op->fout,
				"\"%s\" [label=\"%s\"];\n",
				st_opts->pseudo,
				state_name
			);
}

int graphviz_exporter 	(	struct container* c,
							FILE* fout,
							int argc,
							char** argv
						)
{
	if (c == NULL)
	{
		ERROR("NULL instead of container");
		return 0;
	}
	else if (c->type != LABELED_DIGRAPH_CONTAINER)
	{
		ERROR("This container type is not supported.");
		return 0;
	}

	struct output_opts op = {
		fout,
		1,
		((struct labeled_digraph*) c)->max_link_count
	};
	
	for (; *argv != NULL; argv++)
	{
		if (sscanf(*argv, "weight=%i",&(op.print_weight)) != 1)
		{
			ERROR("Unsupported param.");
			return 0;
		}
	}
	
	fputs("digraph g {\n", fout);
	
	g_hash_table_foreach(	((struct labeled_digraph*) c)->links,
							(GHFunc)&write_link,
							&op
						);
	g_hash_table_foreach(	((struct labeled_digraph*) c)->states,
							(GHFunc)&write_state,
							&op
						);
	
	fputs("}\n",fout);
	
	return 1;
}

int graphviz_exporter_container_types[] =
	{LABELED_DIGRAPH_CONTAINER, NULL_CONTAINER};

struct exporter graphviz_exporter_definition = {
	"graphviz",
	"Outputs TM in graphviz dot format.",
	&graphviz_exporter_container_types,
	&graphviz_exporter
};
