
struct tmp_param_list {};

struct tmp_param_list* param_list_create(void);

int param_list_add_params (struct tmp_param_list* list, char* str);

int param_list_convert(	struct tmp_param_list** list,
						int* argc,
						char*** argv
						);

void free_argv(char*** argv);
