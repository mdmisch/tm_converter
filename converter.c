
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <glib.h>

	
#define STRING_SIZE 1024

	GHashTable*	states;
	FILE*		input_file;
	FILE*		output_file;
	FILE*		redef_file = NULL;
	
	char string[STRING_SIZE];
	
	char state1[STRING_SIZE];
	char state2[STRING_SIZE];
	
	char* cur_char;
	char* next_param;
	
	uint32_t next_state_num = 0;

#define CLOSE_ALL	if (input_file != NULL) \
						fclose(input_file); \
					if (output_file != NULL) \
						fclose(output_file); \
					if (redef_file != NULL) \
						fclose(redef_file); \
					if (states != NULL) \
						g_hash_table_destroy(states);

#define IF_EOL_ERROR(str) if (*str == 0) \
							{ \
								fputs("Incorrect line.\n", stderr); \
								CLOSE_ALL; \
								exit(1); \
							}

void print_state(char* name, FILE* file, FILE* redef_file)
{
	
	char* redef_state;
	
	char* new_state = strdup(name);
	
	if (!(redef_state = g_hash_table_lookup(states, name)))
	{
		if (strlen(name) < 5)
			redef_state = strdup(name);
		else
		{
			char* b64_state =
				g_base64_encode((char *) &(next_state_num), 4);
			
			redef_state = malloc(6);
			int i;
			for(i = 0; (i < 5) && (b64_state[i] != 0); i++)
				redef_state[i] = b64_state[i];
			redef_state[i] = 0;
			
			g_free(b64_state);
			
			next_state_num++;
		}
		

		g_hash_table_insert(states, new_state, redef_state);

		if (redef_file != NULL)
		{	
			
			fputc('"', redef_file);
			fputs(redef_state, redef_file);
			fputs("\" <- \"", redef_file);
			fputs(name, redef_file);
			fputs("\"\n", redef_file);
		}
		
	}
	
	fputs(redef_state, file);


}

int main(int argc, char** argv)
{
	if ((argc < 3) || (argc > 4))
	{
		fputs("Incorrect params.\n", stderr);
		exit(1);
	}
	
	states = g_hash_table_new_full(	&g_str_hash,
									&g_str_equal,
									&free,
									&free
									);
	
	if (strcmp(*(argv+1), "-") == 0)
		input_file = stdin;
	else
		input_file = fopen(*(argv+1), "r");
	if (input_file == NULL)
	{
		perror("Input file opening error.");
		
		exit(1);
	}
	
	if (strcmp(*(argv+2), "-") == 0)
		output_file = stdout;
	else
		output_file = fopen(*(argv+2), "w");
	
	if (output_file == NULL)
	{
		perror("Output file openng error.");
		CLOSE_ALL;
		exit(1);
	}
	
	if ((argc == 4) && !(redef_file = fopen(*(argv+3), "w")))
	{
			perror("Redefinitions file opening error.");
			CLOSE_ALL;
			exit;
	}
	
	while (fgets(string, STRING_SIZE, input_file))
		if ((string[0] != '#') &&
			(string[0] != 0) &&
			(string[0] != '\n'))
		{
			for(cur_char = string;
				(*cur_char != ',') && (*cur_char != 0);
				cur_char++
				);
			IF_EOL_ERROR(cur_char);
			
			*(cur_char++) = 0;
			
			print_state(string, output_file, redef_file);
			fputc(',', output_file);
			
			next_param = cur_char;
			
			for(;
				(*cur_char != ':') && (*cur_char != 0);
				cur_char++
				);
			IF_EOL_ERROR(cur_char);
			
			*(cur_char++) = 0;
			
			fputs(next_param, output_file);
			fputc(':', output_file);
			
			next_param = cur_char;
			
			for(;
				(*cur_char != ',') && (*cur_char != 0);
				cur_char++
				);
			IF_EOL_ERROR(cur_char);
			
			*(cur_char++) = 0;
			print_state(next_param, output_file, redef_file);
			
			fputc(',', output_file);
			fputs(cur_char, output_file);
			
		}
	if (!feof(input_file))
	{
		perror("File reading error.");
		CLOSE_ALL;
		exit(1);
	}
	CLOSE_ALL;
	
	return 0;
}
