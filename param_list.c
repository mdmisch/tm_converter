
#include <string.h>
#include <stddef.h>
#include <stdlib.h>

#include "param_list.h"

struct reversed_string_list {
	char* str;
	struct reversed_string_list* prev;
};

struct tmp_param_list_def {
	unsigned int size;
	struct reversed_string_list* list;
};

struct tmp_param_list* param_list_create(void)
{
	struct tmp_param_list_def* pl = malloc(sizeof(*pl));
	pl->size = 0;
	pl->list = NULL;
	
	return (struct tmp_param_list*) pl;
}

int param_list_add_params (struct tmp_param_list* list, char* str)
{
	if ((str == NULL) || (*str == 0))
		return 0;
	
	char* cur_param = str;
	
	while(1)
	{
		size_t param_size = strcspn(cur_param, ",");
		
		struct reversed_string_list* nl = malloc(sizeof(*nl));
		
		nl->str = malloc(param_size + 1);
		memcpy(nl->str, cur_param, param_size);
		nl->str[param_size] = 0;
		
		nl->prev = ((struct tmp_param_list_def*) list)->list;
		((struct tmp_param_list_def*) list)->list = nl;
		(((struct tmp_param_list_def*) list)->size)++;
		
		cur_param += param_size;
		
		if (*cur_param == '\0')
			break;
		else
			cur_param++;
	}
	
	return 1;
}

int param_list_convert(	struct tmp_param_list** list,
						int* argc,
						char*** argv
						)
{
	if ((list == NULL) || (argc == NULL) || (argv == NULL))
		return 1;
	
	*argc = ((struct tmp_param_list_def*) (*list))->size;
	*argv = malloc((*argc + 1)*sizeof(void*));
	(*argv)[*argc] = NULL;
	
	struct reversed_string_list* cur_str_list =
		((struct tmp_param_list_def*) (*list))->list;
	for(int i = *argc - 1; i >= 0; i--)
	{
		(*argv)[i] = cur_str_list->str;
		struct reversed_string_list* next_str_list = 
			cur_str_list;
		cur_str_list = cur_str_list->prev;
		free(next_str_list);
	}
	
	free(*list);
	*list = NULL;
	
	return 0;
}

void free_argv(char*** argv)
{
	for (char** buf = *argv; *buf != NULL; buf++)
		free(*buf);
	free(*argv);
	*argv = NULL;
}
