
#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include "tm_modules.h"

#include "parsers/parsers.h"

struct parser* get_parser_by_name(char* name)
{
	if (name == NULL)
		return NULL;
	
	unsigned int i;
	for (	i = 0;
			(parsers[i] != NULL) &&
			(strcmp(parsers[i]->name, name));
			i++
		);
	return parsers[i];
}

int print_parser_description(char* name, FILE* output)
{
	if (name)
	{
		struct parser* p = get_parser_by_name(name);
		if (p == NULL)
		{
			fputs("Unknown parser.\n", output);
			return 0;
		}
		else
		{
			fputs(p->name, output);
			fputs(" - ", output);
			fputs(p->description, output);
			fputc('\n', output);
			return 0;
		}
	}
	else
	{
		for	(	int i = 0;
				parsers[i] != NULL;
				i++
			)
		{
			fputs(parsers[i]->name, output);
			fputs(" - ", output);
			fputs(parsers[i]->description, output);
			fputs("\n\n", output);
		}
		return 0;
	}
}	
