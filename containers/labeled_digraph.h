
#include <glib.h>

#include "tm_modules.h"

extern struct container_descr labeled_digraph_descr;

struct labeled_digraph {
	int type;
	unsigned long long int max_link_count;
	unsigned long long int next_state_id;
	GHashTable* states;
	GHashTable* links;
	GStringChunk* str_chunk;
};


struct link {
	char* st_from;	/* state from */
	char* st_to;	/* state to*/
};

struct state_options {
	unsigned long long int links;
	char* pseudo;
};

struct char_seq {
	char* chr_before;
	char* chr_after;
	char* direction;
	struct char_seq* next;
};

struct link_desc {
	struct state_options* st_from_opts;
	struct state_options* st_to_opts;
	struct char_seq* first;
	struct char_seq* last;
};
