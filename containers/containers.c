
#include <stddef.h>

#include "tm_modules.h"

#include "containers/labeled_digraph.h"

struct container_descr *containers[] = {
	&labeled_digraph_descr,
	NULL
};
