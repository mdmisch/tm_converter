
#include "containers/containers.h"
#include "containers/labeled_digraph.h"

#include <glib.h>
#include <string.h>

#define DEFAULT_CHUNK_SIZE 128

gboolean state_options_destroy(struct state_options* st_opt)
{
	g_free(st_opt->pseudo);
	g_free(st_opt);
}

guint link_hash(struct link* l)
{
	return g_str_hash(l->st_from) ^ g_str_hash(l->st_to);
}

gboolean link_equal(struct link* l1,struct link* l2)
{
	return	!strcmp(l1->st_from, l2->st_from) &&
			!strcmp(l1->st_to, l2->st_to);
}

void link_desc_destroy (struct link_desc* ld)
{
	struct char_seq* next = ld->first;
	while (next != NULL)
	{
		struct char_seq* prev = next;
		next = prev->next;
		g_free(next);
	}
	g_free(ld);
}

struct container* labeled_digraph_constructor (void)
{
	struct labeled_digraph* ldg =
		g_malloc(sizeof(*ldg));
	
	ldg->type = LABELED_DIGRAPH_CONTAINER;
	ldg->max_link_count = 1;
	ldg->next_state_id = 0;
	
	ldg->states = g_hash_table_new_full(	&g_str_hash,
											&g_str_equal,
											NULL,
											(GDestroyNotify)
												&state_options_destroy
										);
	/* key is a state name, saved in str_chunk,
	 * value is a struct state_options */
	
	ldg->links = g_hash_table_new_full
								(	(GHashFunc)&link_hash,
									(GEqualFunc)&link_equal,
									&g_free,
									(GDestroyNotify)&link_desc_destroy
								);
	/* Key is a `struct link`, value is link's description as GString.*/
								
	ldg->str_chunk = g_string_chunk_new(DEFAULT_CHUNK_SIZE);
	/* Here we will store state names.*/
	
	return (struct container*) ldg;
}


struct state_options* inc_state_counter(struct labeled_digraph* c,
										char* state
										)
{
	struct state_options* st_opts = 
		g_hash_table_lookup(c->states, state);
	if (! st_opts)
	{
		st_opts = g_malloc(sizeof(*st_opts));
		st_opts->links = 1;
		st_opts->pseudo = g_strdup_printf("%llx", c->next_state_id++);
		
		g_hash_table_insert(c->states, state, st_opts);
	}
	else
		(st_opts->links)++;
	
	if (st_opts->links > c->max_link_count)
		c->max_link_count = st_opts->links;
	
	return st_opts;
}

int add_link_description(	struct labeled_digraph* container,
							char* st_from,
							char* st_to,
							char* chr_before,
							char* chr_after,
							char* direction
						)
{
	struct link l = {
		st_from,
		st_to
	};
	
	struct link_desc* desc = g_hash_table_lookup(container->links, &l);
	
	if (! desc)
	{
		desc = g_malloc(sizeof(*desc));
		desc->first = desc->last = g_malloc(sizeof(*desc->last));

		struct link* new_link = g_malloc(sizeof(*new_link));

		new_link->st_from =
			g_string_chunk_insert_const(container->str_chunk, st_from);	
		new_link->st_to =
			g_string_chunk_insert_const(container->str_chunk, st_to);
		
		desc->st_from_opts =
			inc_state_counter(container, new_link->st_from);
		desc->st_to_opts =
			inc_state_counter(container, new_link->st_to);
		
		g_hash_table_insert(container->links, new_link, desc);
	}
	else
	{
		desc->last->next = g_malloc(sizeof(*desc->last->next));
		desc->last = desc->last->next;
	}
	
	desc->last->chr_before =
		g_string_chunk_insert_const(container->str_chunk,
									chr_before
								   );
	desc->last->chr_after =
		g_string_chunk_insert_const(container->str_chunk,
									chr_after
								   );
	desc->last->direction =
		g_string_chunk_insert_const(container->str_chunk,
									direction
								   );
	desc->last->next = NULL;

	return 1;
}

int labeled_digraph_destrucror (struct labeled_digraph* container)
{
	g_hash_table_destroy(container->states);
	g_hash_table_destroy(container->links);
	g_string_chunk_free(container->str_chunk);
	
	g_free(container);
	
	return 1;
}


struct container_descr labeled_digraph_descr = 
{
	LABELED_DIGRAPH_CONTAINER,
	(parser_callback_func) &add_link_description,
	(container_construct_func) &labeled_digraph_constructor,
	(container_destroy_func) &labeled_digraph_destrucror,
	"labeled_digraph",
	"This cotainer is designed as a storage which provide a\n"
	"simple way to search commands by before- and after- state\n"
	"combinations. It also internally counts number of such pairs in\n"
	"which avery state is involved and creates short pseudonyms for\n"
	"them.\n"
};

