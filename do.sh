#!/bin/sh
#cc `pkg-config --libs --cflags glib-2.0` --std=gnu99 -ggdb -o converter converter.c
cc -g -std=c99 -I. `pkg-config --libs --cflags glib-2.0` graphviz_convert.c tm_*.c param_list.c parsers/*.c containers/*.c exporters/*.c
